#!/usr/bin/env python
# coding: utf-8

# # Scientific Computing - mini-projet : Etude de la méthode de Newton

# Pour ce mini-projet, on considére la méthode de Newton aussi appelée méthode de Newton-Raphtson. Cette méthode développée par Newton permet de trouver les racines réelles de polynômes. Elle possède des avantages et des inconveignents qui seront étudiés par la suite. Elle sera aussi comparée à d'autres méthodes numériques.
# 
# 
# 
# 
# **1)** Equation historique de Newton :
# 
# Newton a trouvé sa méthode en se donnant pour objectif de résoudre l'équation suivante :
# 
# 
# \begin{equation}
# x^3 - 2x -5 = 0
# \end{equation}
# 
# 
# Dans un premier temps, on trace la fonction correspondante pour prouver qu'elle possède bien une unique solution réelle x comprise entre 2 et 2.2. On appellera par la suite cette fonction la fonction historique de Newton.

# In[228]:


# On importe les librairies utilisées
import matplotlib.pyplot as plt
import numpy as np
import numpy.polynomial.polynomial as poly


# In[229]:


# Definition de la fonction historique de Newton
def fhistoriquenewton(x):
    z = np.power(x,3)-2*x-5
    return z


# In[230]:


# Definition d'une fonction permettant de tracer la fonction historique de Newton
def plot_function(x,func):
    plt.plot(x, func(x))
    plt.xlabel("x")
    plt.ylabel("f(x)")
    plt.grid()
    plt.show()
    return


# In[231]:


# On appelle la fonction plot_function pour représenter la fonction historique de Newton
plot_function(np.linspace(-10,10), fhistoriquenewton)
plot_function(np.linspace(-5,5), fhistoriquenewton)


# Comme on peut le constater, la fonction ne s'annule qu'en un unique point x proche de la valeur 2.
#                                 

# **2)** Definition de la méthode de Newton

# En principe, lorsque l'on s'approche d'une racine du polynome, la fonction est proche de zéro et tend vers zéro. On peut donc réaliser un développement limité en ce point.
# 
# 
# On a :
# 
# \begin{equation}
# f(x)\approx f(xo) + f'(xo)(x-xo)
# \end{equation}
# 
# 
# 
# De plus, vu que l'on recherche les racines du polynome, c'est-à-dire les valeurs pour lesquelles la fonction s'annule, on peut poser : 
# 
# \begin{equation}
# f(x)= 0
# \end{equation}
# 
# Ce qui donne : 
# 
# \begin{equation}
# f(xo) + f'(xo)(x-xo)= 0
# \end{equation}
# 
# On trouve alors une equation permettant de trouver une valeur de x :
# 
# 
# 
# \begin{equation}
# x = xo - \frac{f(xo)}{f'(xo)}
# \label{f1}
# \end{equation}
# 
# 
# En passant à l'algorithme, on obtient donc : 
# 
# \begin{equation}
# x[i+1] = x[i] - \frac{f(x[i])}{f'(x[i])}
# \label{f2}
# \end{equation}
# 
# 
# On peut constater que cette méthode permet de trouver par itération une nouvelle valeur de x s'approchant du point d'annulation de la fonction et qu'elle fait appel à la dérivée de la fonction. Il faut donc vérifier que la fonction dérivée ne s'annulle pas pendant le calcul. C'est l'une des conditions à toujours vérifier.

# On va donc déterminer la fonction dérivée et la tracer pour trouver ses points d'annulation.

# In[232]:


# Definition de la fonction dérivée de Newton
def fhistoriquenewton_derive(x):
    z = 3*np.power(x,2)-2
    return z


# In[233]:


# Représentation de la fonction dérivée de Newton
plot_function(np.linspace(-5,5), fhistoriquenewton_derive)


# La fonction dérivée est une parabole s'annulant deux fois à proximité de 1 et de -1. Il faut donc éviter, pour l'initialisation du calcul de passer à proximité de ces points. On va maintenant programmer l'algorithme de la méthode de Newton.

# La méthode de Newton a pour intérêt de présenter une convergence quadratique d'ordre 2. Ce qui veut dire que la méthode converge très vite. Malheureusement, la méthode peut diverger et nécessite un point d'initialisation proche de la  racine. 
# 
# L'algorithme va donc prendre en argument, la fonction, sa dérivée ainsi qu'un point d'initialisatin du calcul. On ne va  effectuer que dix itérations : si le calcul n'a pas convergé au bout de dix itérations, on considera que le calcul ne converge pas.

# In[234]:


# Defintition de l'algorithme de Newton
def methode_de_Newton(xo,func,funcderive):
    #initialisation de la méthode
    x = np.zeros(10)
    x[0] = xo
    for i in range(0,9):
        x[i+1] = x[i] - (func(x[i]))/(funcderive(x[i]))
        x[i] = x[i+1]
        #print("", x)
    return x


# On applique la méthode en prenant un point d'initialisation de 2.

# In[235]:


racinex = methode_de_Newton(2,fhistoriquenewton, fhistoriquenewton_derive)
print(racinex)


# On constate que la convergence est très rapide puisque la racine est atteinte en trois itérations seulement et le calcul ne bouge plus pas la suite.
# La racine du polynôme vaut donc 2.09455148. On peut tester la méthode avec d'autres conditions initiales.

# In[236]:


racinex = methode_de_Newton(5,fhistoriquenewton, fhistoriquenewton_derive)
print("", racinex)


# En prenant comme condition initiale x=5, la convergence est atteinte au bout de six itérations.

# In[237]:


racinex2 = methode_de_Newton(10,fhistoriquenewton, fhistoriquenewton_derive)
print("", racinex2)
racinex2 = methode_de_Newton(15,fhistoriquenewton, fhistoriquenewton_derive)
print("", racinex2)
racinex2 = methode_de_Newton(20,fhistoriquenewton, fhistoriquenewton_derive)
print("", racinex2)
racinex2 = methode_de_Newton(50,fhistoriquenewton, fhistoriquenewton_derive)
print("", racinex2)
racinex2 = methode_de_Newton(100,fhistoriquenewton, fhistoriquenewton_derive)
print("", racinex2)
racinex2 = methode_de_Newton(500,fhistoriquenewton, fhistoriquenewton_derive)
print("", racinex2)


# En prenant des conditions initiales extrêmes pour les x positifs, la méthode semble tout de même toujours converger. On regarde maintenant comme les choses se passent pour les x négatifs.

# In[238]:


racinex2 = methode_de_Newton(-2,fhistoriquenewton, fhistoriquenewton_derive)
print("", racinex2)
racinex2 = methode_de_Newton(-5,fhistoriquenewton, fhistoriquenewton_derive)
print("", racinex2)
racinex2 = methode_de_Newton(-10,fhistoriquenewton, fhistoriquenewton_derive)
print("", racinex2)
racinex2 = methode_de_Newton(-15,fhistoriquenewton, fhistoriquenewton_derive)
print("", racinex2)
racinex2 = methode_de_Newton(-50,fhistoriquenewton, fhistoriquenewton_derive)
print("", racinex2)
racinex2 = methode_de_Newton(-100,fhistoriquenewton, fhistoriquenewton_derive)
print("", racinex2)


# On constate qu'en prenant des x négatifs, la solution peut converger mais qu'elle peut aussi diverger en fonction des cas.

# On a pu constater grâce aux exemples ci-dessus, que la méthode de Newton permet de converger très rapidement vers la solution, quand la méthode converge grâce à sa convergence quadratique.
# 
# Cependant, l'un des problèmes de cette méthode est qu'elle nécessite que la fonction dérivée ne s'annule pas pendant le calcul. A l'époque de Newton, la dérivée n'existait pas et il réalisait un calcul approché. Dans ce qui suit, on va mettre en place la méthode dite de la sécante qui est une méthode analogue à la méthode de Newton, mais ne faisant pas appel à la fonction dérivée.
# 

# **3)** Méthode dite de la sécante
# 
# Cette méthode a pour grand avantage de remplacer la fonction dérivée par une approximation. Ainsi le risque d'obtenir une forme indéterminée n'existe plus. On remplace en effet la dérivée de la fonction par une approximation utilisant une méthode aux différences finies.
# 
# Cette méthode ne possède cependant plus la convergence quadratique de la méthode de Newton, mais une convergence de d'ordre 1.618 qui est le nombre d'or.
# 
# La dérivée se définit telle que :
# 
# \begin{equation}
# f'(x)= \frac{f(x+\epsilon)-f(x)}{(x+\epsilon)-x}
# \end{equation}
# 
# Soit :
# 
# \begin{equation}
# f'(x)= \frac{f(x+\epsilon)-f(x)}{\epsilon}
# \end{equation}
# 
# On peut donc à partir de cette définition, remplacer la dérivée par une méthode aux différences finies et on obtient :
# 
# \begin{equation}
# f'(x)= \frac{f(x[i]-f(x[i-1])}{x[i]-x[i-1]]}
# \end{equation}
# 
# L'algorithme de la méthode de Newton devient simplement : 
# 
# \begin{equation}
# x[i+1]= x[i] - \frac{(x[i]-(x[i-1])}{f(x[i])-f(x[i-1])}f(x[i])
# \end{equation}
# 
# On peut voir que cette méthode prend comme argument deux valeurs de x pour conditions initiales.

# In[249]:


# Définition de la méthode dite de la sécante
def methode_de_la_secante(xo,x1,func):
    # Initialisation de la méthode
    x = np.zeros(10)
    x[0] = xo
    x[1] = x1
    for i in range(1,9):
        #A = x[i] - x[i-1]
        #B = func(x[i]) - func(x[i-1])
        x[i+1] = x[i] - ((x[i]-x[i-1])/(func(x[i]) - func(x[i-1])))*func(x[i])
        #x[i+1] = x[i] - (A/B)*func(i)
        x[i] = x[i+1]
        #print("", x)
    return x


# In[250]:


racinex = methode_de_la_secante(2, 2.1, fhistoriquenewton)
print("", racinex)


# 4) Méthode de Dichotomie
# 
# Pour finir, on va considérer une dernière méthode pour trouver des racines. Il s'agit de la méthode de Dichotomie qui est une méthode assez simple mais dont la convergence est seulement d'ordre 1. C'est aussi une méthode qui a des difficultés à trouver des racines quand elles sont proches l'une de l'autre.
# 
# Si on se place dans un intervalle où la fonction s'annule et qu'on prend deux points a et b de par d'autre de la racine, on sait que la solution est comprise dans l'intervale [a,b] en vertu du théorème des valeurs intermédiaires.
# 

# In[241]:


# Definition de la méthode de Dichotomie

# Pour commencer, on doit définir une tolérance d'arrêt de la méthode
# Cette tolérance est représentée par epsilon et constitue l'écart entre les deux bornes a et b
# On considère que le calcul a convergé vers la solution une fois que epsilon est atteint

def dichotomie(borne1, borne2, epsilon, func):
    xg = borne1
    xd = borne2
    while (np.abs(xg-xd) >= epsilon):
        c = (xg+xd)/2.0
        prod = func(xg)*func(c)
        if prod > epsilon:
            xg = c
        else :
            if prod < epsilon:
                xd = c
    return c

racinex = dichotomie(2, 2.2, 0.1, fhistoriquenewton)
print("", racinex)


# On constate qu'avec un epsilon de 0.1, la méthode tend à converger vers la solution.

# In[242]:


racinex = dichotomie(2, 2.2, 0.01, fhistoriquenewton)
print("", racinex)
racinex = dichotomie(2, 2.2, 0.005, fhistoriquenewton)
print("", racinex)
racinex = dichotomie(2, 2.2, 0.0025, fhistoriquenewton)
print("", racinex)
racinex = dichotomie(2, 2.2, 0.001, fhistoriquenewton)
print("", racinex)
racinex = dichotomie(2, 2.2, 0.0001, fhistoriquenewton)
print("", racinex)
racinex = dichotomie(2, 2.2, 0.00001, fhistoriquenewton)
print("", racinex)
racinex = dichotomie(2, 2.2, 0.000001, fhistoriquenewton)
print("", racinex)
racinex = dichotomie(2, 2.2, 0.0000001, fhistoriquenewton)
print("", racinex)
racinex = dichotomie(2, 2.2, 0.00000001, fhistoriquenewton)
print("", racinex)
racinex = dichotomie(2, 2.2, 0.0000000001, fhistoriquenewton)
print("", racinex)


# En descendant progressivement la valeur de epsilon, on arrive à converger vers la solution. Il faut cependant un epsilon très très faible pour obtenir la même solution trouvée avec la méthode de Newton (2.09455148)

# 5) Built-in function
# 
# Il est possible d'utiliser une fonction faisant partie de la librairie de numpy pour calculer les racines d'un polynome.
# 
# La fonction Polyroots rend les racines des polynomes qu'on lui donne. Il faut donner dans l'ordre croissant du degré des monomes le coefficient associé.

# In[243]:


# Utilisation de la fonction Polyroots pour résoudre le polynome de Newton
s = (-5, -2, 0, 1)
res = poly.polyroots(s)
print(res)


# On obtient bien trois solutions dont deux solutions complexes et une solution réelle. Cette solution réelle vaut 2.09455148 ce qui est bien la valeur obtenue avec la méthode de Newton. Cette built-in fonction permet donc de confirmer la méthode de Newton présentement développée.

# # Scientific Computing - mini-projet : Application de la méthode de Newton à l'attracteur de Lorenz

# In[ ]:




